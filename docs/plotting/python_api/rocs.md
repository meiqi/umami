# ROC curve plotting API
In the following a small example how to plot a roc curve with the umami python api.

To set up the inputs for the plots, have a look [here](./index.md).

Then we can start the actual plotting part


???+ example "ROC plot code"
    ![rocs](../../ci_assets/roc.png)
    ```py linenums="1"
    §§§examples/plotting/plot_rocs.py§§§
    ```