# Evaluation parameters
Eval_parameters:
  Path_to_models_dir: <path_palce_holder>/umami/umami
  model_name: dips_Loose_lr_0.001_bs_15000_epoch_200_nTrainJets_Full
  epoch: 59
  epoch_to_name: True

# Contour fraction plot
contour_fraction_ttbar:
  type: "Frac_Contour"
  rejections: ["ujets", "cjets"]
  models_to_plot:
    dips:
      tagger_name: "dips"
      colour: "b"
      linestyle: "--"
      label: "DIPS"
      data_set_name: "ttbar_r21"
      marker:
        cjets: 0.1
        ujets: 0.9
        marker_style: "x"
    rnnip:
      tagger_name: "rnnip"
      colour: "r"
      linestyle: "--"
      label: "RNNIP"
      data_set_name: "ttbar_r21"
  plot_settings:
    yAxisIncrease: 1.3 # Increasing of the y axis so the plots dont collide with labels (mainly AtlasTag)
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets,\n$t\\bar{t}$ Test Sample, WP = 77 %"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag

# Dips, ttbar
scores_Dips_ttbar:
  type: "scores"
  data_set_name: "ttbar_r21" # data set to use. This is the dict entry name of the file you want to plot in the train config
  tagger_name: "dips" # Name of the tagger: Example: dips_pb -> Name of tagger: dips
  class_labels: ["ujets", "cjets", "bjets"] # Classes which are used
  main_class: "bjets" # Main class
  plot_settings:
    WorkingPoints: [0.60, 0.70, 0.77, 0.85] # Set Working Point Lines in plot
    nBins: 50 # Number of bins
    yAxisIncrease: 1.3 # Increasing of the y axis so the plots dont collide with labels (mainly AtlasTag)
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets,\n$t\\bar{t}$ Test Sample"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag

# Dips, ttbar
scores_Dips_ttbar_comparison:
  type: "scores_comparison"
  main_class: "bjets"
  models_to_plot:
    dips_r21:
      data_set_name: "ttbar_r21"
      tagger_name: "dips"
      class_labels: ["ujets", "cjets", "bjets"]
      label: "$t\\bar{t}$"
    dips_r22: # The name here has no impact on anything.
      data_set_name: "ttbar_r21"
      tagger_name: "dips"
      class_labels: ["ujets", "cjets", "bjets"]
      label: "$t\\bar{t} 2$"
  plot_settings:
    WorkingPoints: [0.60, 0.70, 0.77, 0.85] # Set Working Point Lines in plot
    nBins: 50 # Number of bins
    yAxisIncrease: 1.4 # Increasing of the y axis so the plots dont collide with labels (mainly AtlasTag)
    figsize: [8, 6] # [width, hight]
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag
    Ratio_Cut: [0.5, 1.5]

Dips_pT_vs_beff:
  type: "pT_vs_eff"
  models_to_plot:
    dips:
      data_set_name: "ttbar_r21"
      label: "DIPS"
      tagger_name: "dips"
  plot_settings:
    bin_edges: [0, 20, 30, 40, 60, 85, 110, 140, 175, 250, 400, 1000]
    flavour: "cjets"
    class_labels: ["ujets", "cjets", "bjets"]
    main_class: "bjets"
    WP: 0.77
    WP_Line: True
    Fixed_WP_Bin: False
    figsize: [7, 5]
    logy: False
    UseAtlasTag: True
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets,\n$t\\bar{t}$ Test Sample"
    yAxisAtlasTag: 0.9
    yAxisIncrease: 1.3

Dips_light_flavour_ttbar:
  type: "ROC"
  main_class: "bjets"
  models_to_plot:
    dips_r21: # The name here has no impact on anything.
      data_set_name: "ttbar_r21"
      label: "DIPS"
      tagger_name: "dips"
      rejection_class: "cjets"
  plot_settings:
    binomialErrors: True
    xmin: 0.5
    ymax: 1000000
    figsize: [7, 6] # [width, hight]
    WorkingPoints: [0.60, 0.70, 0.77, 0.85]
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets,\n$t\\bar{t}$ Validation Sample, fc=0.018"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag

Dips_Comparison_flavour_ttbar:
  type: "ROC_Comparison"
  models_to_plot:
    dips_r21_u:
      data_set_name: "ttbar_r21"
      label: "DIPS"
      tagger_name: "dips"
      rejection_class: "ujets"
    dips_r21_c:
      data_set_name: "ttbar_r21"
      label: "DIPS"
      tagger_name: "dips"
      rejection_class: "cjets"
  plot_settings:
    binomialErrors: True
    xmin: 0.5
    ymax: 1000000
    figsize: [9, 9] # [width, hight]
    WorkingPoints: [0.60, 0.70, 0.77, 0.85]
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets,\n$t\\bar{t}$ Validation Sample, fc=0.018"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag

confusion_matrix_Dips_ttbar:
  type: "confusion_matrix"
  data_set_name: "ttbar_r21"
  tagger_name: "dips"
  class_labels: ["ujets", "cjets", "bjets"]
  plot_settings:
    colorbar: True

Dips_saliency_b_WP77_passed_ttbar:
  type: "saliency"
  data_set_name: "ttbar_r21"
  plot_settings:
    title: "Saliency map for $b$ jets from \n $t\\bar{t}$ who passed WP = 77% \n with exactly 8 tracks"
    target_beff: 0.77
    jet_flavour: "cjets"
    PassBool: True
    FlipAxis: True
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets"
    yAxisAtlasTag: 0.925

# Dips, ttbar
Dips_prob_pb:
  type: "probability"
  data_set_name: "ttbar_r21"
  tagger_name: "dips"
  class_labels: ["ujets", "cjets", "bjets"]
  prob_class: "bjets"
  plot_settings:
    logy: True
    nBins: 50
    yAxisIncrease: 10 # Increasing of the y axis so the plots dont collide with labels (mainly AtlasTag)
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets,\n$t\\bar{t}$ Test Sample"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag

Dips_prob_comparison_pb:
  type: "probability_comparison"
  prob_class: "bjets"
  models_to_plot:
    dips_r22:
      data_set_name: "ttbar_r21"
      label: "RNNIP"
      tagger_name: "dips"
      class_labels: ["ujets", "cjets", "bjets"]
    dips_r21:
      data_set_name: "ttbar_r21"
      label: "DIPS 2"
      tagger_name: "dips"
      class_labels: ["ujets", "cjets", "bjets"]
  plot_settings:
    nBins: 50 # Number of bins
    logy: True
    yAxisIncrease: 1.5 # Increasing of the y axis so the plots dont collide with labels (mainly AtlasTag)
    figsize: [8, 6] # [width, hight]
    UseAtlasTag: True # Enable/Disable AtlasTag
    AtlasTag: "Internal Simulation"
    SecondTag: "\n$\\sqrt{s}=13$ TeV, PFlow Jets"
    yAxisAtlasTag: 0.9 # y axis value (1 is top) for atlas tag
    Ratio_Cut: [0.5, 1.5]
