.test_template: &test_template
  before_script:
    - . run_setup.sh
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_PROJECT_PATH=="atlas-flavor-tagging-tools/algorithms/umami"
  image: "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/umami/$IMAGE_TYPE"
  dependencies:
    - build_umamibase_cpu_MR
    - build_umamibase_cpu
    - linter
  needs:
    - job: build_umamibase_cpu_MR
      optional: true
    - job: build_umamibase_cpu
      optional: true
    - linter
  retry: 2


.artifact_template: &artifact_template
  name: "$CI_JOB_NAME"
  expire_in: 1 day
  reports:
    junit: report.xml


.dependencies_from_train_stage: &dependencies_from_train_stage
  - test_train_dips
  - test_train_dips_four_classes
  - test_train_tfrecords_dips
  - test_train_cads
  - test_train_tfrecords_cads
  - test_train_dl1r
  - test_train_umami
  - test_train_tfrecords_umami
  - test_train_cond_att_umami
  - test_train_tfrecords_cond_att_umami
  - test_evaluate_tagger_in_files


test_preprocessing_dips_count:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dips_count" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dips_count
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dips/
      - coverage_files/

test_preprocessing_dl1r_count:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dl1r_count" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dl1r_count
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dl1r/
      - coverage_files/

test_preprocessing_umami_count:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_umami_count" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_umami_count
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_umami/
      - coverage_files/

test_preprocessing_dips_pdf:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dips_pdf" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dips_pdf
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dips_pdf/
      - coverage_files/

test_preprocessing_dips_four_classes_pdf:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dips_four_classes_pdf" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dips_four_classes_pdf
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dips/
      - coverage_files/

test_preprocessing_dl1r_pdf:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dl1r_pdf" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dl1r_pdf
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dl1r_pdf/
      - coverage_files/

test_preprocessing_umami_pdf:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_umami_pdf" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_umami_pdf
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_umami_pdf/
      - coverage_files/

test_preprocessing_dips_weighting:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dips_weighting" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dips_weighting
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dips_weighting/
      - coverage_files/

test_preprocessing_dl1r_weighting:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_dl1r_weighting" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_dl1r_weighting
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_dl1r_weighting/
      - coverage_files/

test_preprocessing_umami_weighting:
  <<: *test_template
  stage: integration_test_preprocessing
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_preprocessing.py -k "test_preprocessing_umami_weighting" -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_preprocessing_umami_weighting
  artifacts:
    <<: *artifact_template
    paths:
      - plots/
      - test_preprocessing_umami_weighting/
      - coverage_files/

test_train_dips:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_dips_count
    - test_preprocessing_dips_pdf
    - test_preprocessing_dips_weighting
  dependencies:
    - test_preprocessing_dips_count
    - test_preprocessing_dips_pdf
    - test_preprocessing_dips_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_dips_no_attention"
    - cp .coverage ./coverage_files/.coverage.test_train_dips
  artifacts:
    <<: *artifact_template
    paths:
      - test_dips_model/
      - coverage_files/

test_train_dips_four_classes:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_dips_four_classes_pdf
  dependencies:
    - test_preprocessing_dips_four_classes_pdf
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_dips_four_classes"
    - cp .coverage ./coverage_files/.coverage.test_train_dips_four_classes
  artifacts:
    <<: *artifact_template
    paths:
      - test_dips_model_four_classes/
      - coverage_files/

test_train_tfrecords_dips:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_dips_count
    - test_preprocessing_dips_pdf
    - test_preprocessing_dips_weighting
  dependencies:
    - test_preprocessing_dips_count
    - test_preprocessing_dips_pdf
    - test_preprocessing_dips_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_tfrecords_dips"
    - cp .coverage ./coverage_files/.coverage.test_train_tfrecords_dips
  artifacts:
    <<: *artifact_template
    paths:
      - test_dips_model_tfrecords/
      - coverage_files/

test_train_cads:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  dependencies:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_cads"
    - cp .coverage ./coverage_files/.coverage.test_train_cads
  artifacts:
    <<: *artifact_template
    paths:
      - test_cads_model/
      - coverage_files/

test_train_tfrecords_cads:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  dependencies:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_tfrecords_cads"
    - cp .coverage ./coverage_files/.coverage.test_train_tfrecords_cads
  artifacts:
    <<: *artifact_template
    paths:
      - test_cads_model_tfrecords/
      - coverage_files/

test_train_dl1r:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_dl1r_count
    - test_preprocessing_dl1r_pdf
    - test_preprocessing_dl1r_weighting
  dependencies:
    - test_preprocessing_dl1r_count
    - test_preprocessing_dl1r_pdf
    - test_preprocessing_dl1r_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_dl1r"
    - cp .coverage ./coverage_files/.coverage.test_train_dl1r
  artifacts:
    <<: *artifact_template
    paths:
      - test_dl1r_model/
      - coverage_files/

test_train_umami:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  dependencies:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_umami"
    - cp .coverage ./coverage_files/.coverage.test_train_umami
  artifacts:
    <<: *artifact_template
    paths:
      - test_umami_model/
      - coverage_files/

test_train_tfrecords_umami:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  dependencies:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_tfrecords_umami"
    - cp .coverage ./coverage_files/.coverage.test_train_tfrecords_umami
  artifacts:
    <<: *artifact_template
    paths:
      - test_umami_model_tfrecords/
      - coverage_files/

test_train_cond_att_umami:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  dependencies:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_cond_att_umami"
    - cp .coverage ./coverage_files/.coverage.test_train_cond_att_umami
  artifacts:
    <<: *artifact_template
    paths:
      - test_umami_cond_att_model/
      - coverage_files/

test_train_tfrecords_cond_att_umami:
  <<: *test_template
  stage: integration_test_tagger
  needs:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  dependencies:
    - test_preprocessing_umami_count
    - test_preprocessing_umami_pdf
    - test_preprocessing_umami_weighting
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_train_tfrecords_cond_att_umami"
    - cp .coverage ./coverage_files/.coverage.test_train_tfrecords_cond_att_umami
  artifacts:
    <<: *artifact_template
    paths:
      - test_umami_cond_att_model_tfrecords/
      - coverage_files/

test_evaluate_tagger_in_files:
  <<: *test_template
  stage: integration_test_tagger
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_train.py -v -s --junitxml=report.xml -k "test_evaluate_tagger_in_files"
    - cp .coverage ./coverage_files/.coverage.test_evaluate_tagger_in_files
  artifacts:
    <<: *artifact_template
    paths:
      - test_evaluate_comp_taggers_model/
      - coverage_files/

test_plot_input_vars:
  <<: *test_template
  stage: integration_test_plotting
  needs: *dependencies_from_train_stage
  dependencies: *dependencies_from_train_stage
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_input_vars_plot.py -v -s --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_input_vars_plot
  artifacts:
    <<: *artifact_template
    paths:
      - input_vars_jets/
      - input_vars_trks/
      - coverage_files/

test_plotting_umami_dips:
  <<: *test_template
  stage: integration_test_plotting
  needs:
    - test_train_dips
    - test_train_tfrecords_dips
  dependencies:
    - test_train_dips
    - test_train_tfrecords_dips
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_plotting_umami.py -v -s -k "test_plotting_umami_dips" --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_plotting_umami_dips
  artifacts:
    <<: *artifact_template
    paths:
      - test_dips_model/
      - coverage_files/

test_plotting_umami_dl1:
  <<: *test_template
  stage: integration_test_plotting
  needs:
    - test_train_dl1r
  dependencies:
    - test_train_dl1r
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_plotting_umami.py -v -s -k "test_plotting_umami_dl1r" --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_plotting_umami_dl1r
  artifacts:
    <<: *artifact_template
    paths:
      - test_dl1r_model/
      - coverage_files/

test_plotting_umami_umami:
  <<: *test_template
  stage: integration_test_plotting
  needs:
    - test_train_umami
    - test_train_tfrecords_umami
  dependencies:
    - test_train_umami
    - test_train_tfrecords_umami
  script:
    - pytest --cov=./ --cov-report= ./umami/tests/integration/test_plotting_umami.py -v -s -k "test_plotting_umami_umami" --junitxml=report.xml
    - cp .coverage ./coverage_files/.coverage.test_plotting_umami_umami
  artifacts:
    <<: *artifact_template
    paths:
      - test_umami_model/
      - coverage_files/

test_examples:
  # should be run early in the pipeline, otherwise we have to wait quite long to see
  # the MR docs
  <<: *test_template
  stage: integration_test_plotting
  script:
    - pytest -v umami/tests/integration/test_examples.py
  artifacts:
    paths:
      - docs/ci_assets
    expire_in: 1 day
